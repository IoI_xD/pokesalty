    db SPECIES_H ; 253

    db 72,  72,  72,   255,  72,  72
;   hp  atk  def  spd  sat  sdf

    db STEEL, PSYCHIC ; type
    db 250 ; catch rate
    db 72 ; base exp
    db LEFTOVERS, LEFTOVERS ; items
    db GENDER_UNKNOWN ; gender ratio
    db 100 ; unknown 1
    db 72 ; step cycles to hatch
    db 5 ; unknown 2
    INCBIN "gfx/pokemon/h/front.dimensions"
    db 0, 0, 0, 0 ; padding
    db GROWTH_NONE ; growth rate
    dn EGG_NONE, EGG_NONE ; egg groups

    ; tm/hm learnset
    tmhm
    ; end
