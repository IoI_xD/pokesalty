_Agreement2::
    text "IMITATIONS AND/O"
    line "R EXCLUSIONS MAY"
    cont " NOT APPLY TO YO"
    cont "U. THIS WARRANTY"
    cont " GIVES YOU SPECI"
    cont "FIC LEGAL RIGHTS"
    cont ", AND YOU MAY HA"
    cont "VE OTHER RIGHTS "
    cont "WHICH VARY FROM "
    cont "JURISDICTION TO "
    cont "sers the right t"
    cont "o use, copy, mod"
    cont "ify, display, pe"
    cont "rform, create de"
    cont "rivative works f"
    cont "rom, and otherwi"
    cont "se communicate a"
    cont "nd distribute yo"
    cont "ur UGC on or thr"
    cont "ough the Program"
    cont " without further"
    cont " notice, attribu"
    cont "tion or compensa"
    cont "tion to you. You"
    cont " may only upload"
    cont " your own UGC to"
    cont " the Program; do"
    cont " not upload anyo"
    cont "ne else’s UGC.  "
    cont "Inactivision reser"
    cont "ves the right (b"
    cont "ut has no obliga"
    cont "tion) to remove,"
    cont " block, edit, mo"
    cont "ve, or disable U"
    cont "GC for any reaso"
    cont "n in Inactivision’"
    cont "s sole discretio"
    cont "n.  Inactivision i"
    cont "s not responsibl"
    cont "e for, and does "
    cont "not endorse or g"
    cont "uarantee, the op"
    cont "inions, views, a"
    cont "dvice or recomme"
    cont "ndations posted "
    cont "or sent by other"
    cont " users."
    cont "For residents of"
    cont " all countries o"
    cont "utside North Ame"
    cont "rica: Users of t"
    cont "he Program creat"
    cont "e, upload, downl"
    cont "oad and use UGC "
    cont "at their own ris"
    cont "k. If you upload"
    cont " or make availab"
    cont "le to other user"
    cont "s your UGC via o"
    cont "ur Program, we d"
    cont "o not control, m"
    cont "onitor, endorse "
    cont "or own your UGC,"
    cont " and you are com"
    cont "missioning us to"
    cont " host and make a"
    cont "vailable such UG"
    cont "JURISDICTION."
    cont "7(B) FOR RESIDEN"
    cont "TS OUTSIDE NORTH"
    cont " AMERICA:"
    cont "NOTHING IN THIS "
    cont "AGREEMENT SHALL "
    cont "LIMIT OR EXCLUDE"
    cont " INACTIVISION'S LI"
    cont "ABILITY TO YOU:"
    cont "FOR DEATH OR PER"
    cont "SONAL INJURY CAU"
    cont "SED BY OUR NEGLI"
    cont "GENCE;"
    cont "FOR FRAUDULENT M"
    cont "ISREPRESENTATION"
    cont "; OR"
    cont "FOR ANY OTHER LI"
    cont "ABILITY THAT MAY"
    cont " NOT, UNDER THE "
    cont "LAWS OF THE JURI"
    cont "SDICTION WHERE Y"
    cont "OU RESIDE, BE LI"
    cont "MITED OR EXCLUDE"
    cont "D."
    cont "SUBJECT TO THIS,"
    cont " IN NO EVENT SHA"
    cont "LL INACTIVISION BE"
    cont " LIABLE TO YOU F"
    cont "OR ANY BUSINESS "
    cont "LOSSES AND ANY L"
    cont "IABILITY ACTIVIS"
    cont "ION DOES HAVE FO"
    cont "R LOSSES YOU SUF"
    cont "FER IS STRICTLY "
    cont "LIMITED TO LOSSE"
    cont "S THAT WERE REAS"
    cont "ONABLY FORESEEAB"
    cont "LE AND SHALL NOT"
    cont ", IN AGGREGATE, "
    cont "EXCEED THE GREAT"
    cont "ER OF THE FOLLOW"
    cont "ING: THE TOTAL P"
    cont "RICE PAID BY YOU"
    cont " FOR THE PROGRAM"
    cont " (PLUS ANY PAID-"
    cont "FOR SERVICE PROV"
    cont "IDED CONTENT) OV"
    cont "ER THE PREVIOUS "
    cont "12-MONTHS FROM T"
    cont "HE DATE ON WHICH"
    cont " THE LIABILITY A"
    cont "RISES; OR THE SU"
    cont "M OF GBP£500 OR "
    cont "EQUIVALENT AMOUN"
    cont "T UNDER THE CURR"
    cont "ENT FOREIGN EXCH"
    cont "ANGE RATE."
    cont "8. TERMINATION: "
    cont "Without prejudic"
    cont "e to any other r"
    cont "ights of Activis"
    cont "ion, this Agreem"
    cont "ent will termina"
    cont "te automatically"
    cont " if you fail to "
    cont "comply with its "
    cont "terms and condit"
    cont "ions. In the eve"
    cont "nt of terminatio"
    cont "n for this reaso"
    cont "n, you must dest"
    cont "roy all copies o"
    cont "f the Program an"
    cont "d all of its com"
    cont "ponent parts.  Y"
    cont "ou may also term"
    cont "inate the Agreem"
    cont "ent at any time "
    cont "by permanently d"
    cont "eleting any inst"
    cont "allation of the "
    cont "Program, and des"
    cont "troying all copi"
    cont "es of the Progra"
    cont "m in your posses"
    cont "sion or control."
    cont " The following p"
    cont "rovisions shall "
    cont "survive terminat"
    cont "ion of this agre"
    cont "ement: LICENSE C"
    cont "ONDITIONS (SECTI"
    cont "ON 3), OWNERSHIP"
    cont " (SECTION 4), LI"
    cont "MITED HARDWARE W"
    cont "ARRANTY (SECTION"
    cont "S 6A AND 6B), LI"
    cont "MITATION OF DAMA"
    cont "GES (SECTIONS 7A"
    cont " AND 7B), TERMIN"
    cont "ATION (SECTION 8"
    cont "nium Copyright A"
    cont "ct ('DMCA'). You"
    cont " are encouraged "
    cont "to review 17 U.S"
    cont ".C. § 512(c)(3) "
    cont "or consult with "
    cont "an attorney prio"
    cont "r to sending a n"
    cont "otice hereunder."
    cont "  To file a copy"
    cont "right infringeme"
    cont "nt notice, you w"
    cont "ill need to send"
    cont " a written commu"
    cont "nication that in"
    cont "cludes the follo"
    cont "wing to the addr"
    cont "ess listed below"
    cont ": (a) your name,"
    cont " address, teleph"
    cont "one number, and "
    cont "email address; ("
    cont "b) a description"
    cont " of the copyrigh"
    cont "ted work that yo"
    cont "u claim has been"
    cont " infringed; (c) "
    cont "the exact URL or"
    cont " a description o"
    cont "f where the alle"
    cont "ged infringing m"
    cont "aterial is locat"
    cont "ed; (d) a statem"
    cont "ent by you that "
    cont "you have a good "
    cont "faith belief tha"
    cont "t the disputed u"
    cont "se is not author"
    cont "ized by the copy"
    cont "right owner, its"
    cont " agent, or the l"
    cont "aw; (e) an elect"
    cont "ronic or physica"
    cont "l signature of t"
    cont "he person author"
    cont "ized to act on b"
    cont "ehalf of the own"
    cont "er of the copyri"
    cont "ght interest; an"
    cont "d (f) a statemen"
    cont "t by you, under "
    cont "penalty of perju"
    cont "ry, that the abo"
    cont "ve information i"
    cont "n your notice is"
    cont " accurate and th"
    cont "at you are the c"
    cont "opyright owner o"
    cont "r authorized to "
    cont "act on the copyr"
    cont "ight owner's beh"
    cont "alf."
    cont "Copyright Agent"
    cont "Inactivision Publi"
    cont "shing, Inc."
    cont "3100 Ocean Park "
    cont "Boulevard"
    cont "Santa Monica, Ca"
    cont "lifornia 90405"
    cont "Attn: Inactivision"
    cont " Business and Le"
    cont "gal Affairs"
    cont "Fax: (310) 255-2"
    cont "152"
    cont "E-Mail: legalaff"
    cont "airs<AT>inactivis"
    cont "ion.com"
    cont "Please note that"
    cont " the DMCA provid"
    cont "), INDEMNITY (SE"
    cont "CTION 10), SERVI"
    cont "CE PROVIDED CONT"
    cont "ENT (SECTION 11)"
    cont ", AVAILABILITY ("
    cont "SECTION 12), ACC"
    cont "ESS (SECTION 13)"
    cont ", BINDING ARBITR"
    cont "ATION AND CLASS "
    cont "ACTION WAIVER (S"
    cont "ECTION 16), JURI"
    cont "SDICTION AND APP"
    cont "LICABLE LAW (SEC"
    cont "TION 17), AND MI"
    cont "SCELLANEOUS (SEC"
    cont "TION 18)."
    cont "9. For residents"
    cont " in North Americ"
    cont "a -- U.S. GOVERN"
    cont "MENT RESTRICTED "
    cont "RIGHTS: The Prog"
    cont "ram has been dev"
    cont "eloped entirely "
    cont "at private expen"
    cont "se and are provi"
    cont "ded as 'Commerci"
    cont "al Computer Soft"
    cont "ware' or 'restri"
    cont "cted computer so"
    cont "ftware.' Use, du"
    cont "plication or dis"
    cont "closure by the U"
    cont ".S. Government o"
    cont "r a U.S. Governm"
    cont "ent subcontracto"
    cont "r is subject to "
    cont "the restrictions"
    cont " set forth in su"
    cont "bparagraph (c)(1"
    cont ")(ii) of the Rig"
    cont "hts in Technical"
    cont " Data and Comput"
    cont "er Software clau"
    cont "ses in DFARS 252"
    cont ".227-7013 or as "
    cont "set forth in sub"
    cont "paragraph (c)(1)"
    cont " and (2) of the "
    cont "Commercial Compu"
    cont "ter Software Res"
    cont "tricted Rights c"
    cont "lauses at FAR 52"
    cont ".227-19, as appl"
    cont "icable. The Cont"
    cont "ractor/Manufactu"
    cont "rer is Activisio"
    cont "n Publishing, In"
    cont "c., 3100 Ocean P"
    cont "ark Boulevard, S"
    cont "anta Monica, Cal"
    cont "ifornia 90405."
    cont "10. For resident"
    cont "s in North Ameri"
    cont "ca -- INDEMNITY:"
    cont " You agree to in"
    cont "demnify, defend,"
    cont " and hold Activi"
    cont "sion, its partne"
    cont "rs, affiliates, "
    cont "licensors, contr"
    cont "actors, officers"
    cont ", directors, emp"
    cont "loyees, and agen"
    cont "ts harmless from"
    cont " all damages, lo"
    cont "sses and expense"
    cont "s arising direct"
    cont "ly or indirectly"
    cont " from your breac"
    cont "h of this Agreem"
    cont "ent and/or your "
    cont "acts and omissio"
    cont "ns in using the "
    cont "Program pursuant"
    cont " to the terms of"
    cont " this Agreement."
    cont "11. SERVICE PROV"
    cont "IDED CONTENT:  '"
    cont "Service Provided"
    cont " Content consis"
    cont "ts of all virtua"
    cont "l materials, inf"
    cont "ormation and con"
    cont "tent provided to"
    cont " you (e.g., unlo"
    cont "ckable content, "
    cont "accounts, stats,"
    cont " virtual assets,"
    cont " virtual currenc"
    cont "ies, codes, achi"
    cont "evements, virtua"
    cont "l rewards, credi"
    cont "ts, access, show"
    cont "s, tokens, coins"
    cont ", power-ups, and"
    cont " customizations)"
    cont " in connection w"
    cont "ith your use of "
    cont "the Program, inc"
    cont "luding the Onlin"
    cont "e Services, whic"
    cont "h you need to 'e"
    cont "arn', 'grind', '"
    cont "buy' and/or 'pur"
    cont "chase' in order "
    cont "to obtain additi"
    cont "onal content."
    cont "While the Progra"
    cont "m may allow you "
    cont "to 'earn,' 'grin"
    cont "d,' 'buy,' or 'p"
    cont "urchase' Service"
    cont " Provided Conten"
    cont "t within or in c"
    cont "onnection with g"
    cont "ameplay, you do "
    cont "not in fact own "
    cont "or have any prop"
    cont "erty interest in"
    cont " the Service Pro"
    cont "vided Content an"
    cont "d the price of a"
    cont "ny Service Provi"
    cont "ded Content does"
    cont " not refer to an"
    cont "y credit balance"
    cont " of real currenc"
    cont "y or its equival"
    cont "ent.  Unless oth"
    cont "erwise specified"
    cont " in writing, any"
    cont " Service Provide"
    cont "d Content that y"
    cont "ou receive is li"
    cont "censed to you as"
    cont " set forth herei"
    cont "n, and you shall"
    cont " have no ownersh"
    cont "ip right thereto"
    cont " in any Service "
    cont "Provided Content"
    cont ". You may not, s"
    cont "ell, lend, rent,"
    cont " trade, or other"
    cont "wise transfer an"
    cont "y Service Provid"
    cont "ed Content, exce"
    cont "pt for other Ser"
    cont "vice Provided Co"
    cont "ntent where appl"
    cont "icable.  Any sal"
    cont "e of Service Pro"
    cont "vided Content, i"
    cont "ncluding, but no"
    cont "t limited to, vi"
    cont "rtual currency f"
    cont "or 'real' money "
    cont "or exchange of t"
    cont "hose items or vi"
    cont "rtual currency f"
    cont "or value outside"
    cont " of the Program "
    cont "is prohibited.  "
    cont " Service Provide"
    cont "d Content may be"
    cont " altered, remove"
    cont "d, deleted, or d"
    cont "iscontinued by Ina"
    cont "ctivision (e.g.,"
    cont " upon terminatio"
    cont "n of this Agreem"
    cont "ent and/or cessa"
    cont "tion of online s"
    cont "upport for the P"
    cont "rogram as set ou"
    cont "t in Section 8) "
    cont "even if you have"
    cont " not 'used' or '"
    cont "consumed' the Se"
    cont "rvice Provided C"
    cont "ontent prior to "
    cont "alteration, remo"
    cont "val, deletion, o"
    cont "r discontinuatio"
    cont "n.  Without limi"
    cont "ting the above, "
    cont "Service Provided"
    cont " Content may inc"
    cont "lude virtual coi"
    cont "ns, points or ot"
    cont "her virtual curr"
    cont "encies ('Virtual"
    cont " Currency')."
    cont "By purchasing or"
    cont " otherwise acqui"
    cont "ring Virtual Cur"
    cont "rency, you obtai"
    cont "n a limited lice"
    cont "nse (which is re"
    cont "vocable by Activ"
    cont "ision at any tim"
    cont "e unless otherwi"
    cont "se required by a"
    cont "pplicable laws) "
    cont "to access and se"
    cont "lect from other "
    cont "Service Provided"
    cont " Content. Virtua"
    cont "l Currency has n"
    cont "o monetary value"
    cont " and does not co"
    cont "nstitute currenc"
    cont "y or property of"
    cont " any type. Virtu"
    cont "al Currency may "
    cont "be redeemed for "
    cont "other Service Pr"
    cont "ovided Content o"
    cont "nly, if at all. "
    cont "Subject to appli"
    cont "cable local law,"
    cont " Virtual Currenc"
    cont "y is non-refunda"
    cont "ble. You are not"
    cont " entitled to a r"
    cont "efund or any oth"
    cont "er compensation "
    cont "such as Service "
    cont "Provided Content"
    cont " for any unused "
    cont "Virtual Currency"
    cont " and unused Virt"
    cont "ual Currency is "
    cont "non-exchangeable"
    cont ".  Inactivision ma"
    cont "y revise the pri"
    cont "cing for the Ser"
    cont "vice Provided Co"
    cont "ntent and Virtua"
    cont "l Currency offer"
    cont "ed through the P"
    cont "rogram at any ti"
    cont "me.  Inactivision "
    cont "may limit the to"
    cont "tal amount of Se"
    cont "rvice Provided C"
    cont "ontent or Virtua"
    cont "l Currency that "
    cont "may be purchased"
    cont " at any one time"
    cont ", and/or limit t"
    cont "he total amount "
    cont "of Service Provi"
    cont "ded Content or V"
    cont "irtual Currency "
    cont "that may be held"
    cont " in your account"
    cont " in the aggregat"
    cont "e.  You are only"
    cont " allowed to purc"
    cont "hase Service Pro"
    cont "vided Content or"
    cont " Virtual Currenc"
    cont "y from Inactivisio"
    cont "n or our authori"
    cont "zed partners thr"
    cont "ough the Program"
    cont ", and not in any"
    cont " other way.  Inact"
    cont "ivision reserves"
    cont " the right to re"
    cont "fuse your reques"
    cont "t(s) to acquire "
    cont "Service Provided"
    cont " Content and/or "
    cont "Virtual Currency"
    cont ".  You agree tha"
    cont "t you will be so"
    cont "lely responsible"
    cont " for paying any "
    cont "applicable taxes"
    cont " related to the "
    cont "acquisition of, "
    cont "use of or access"
    cont " to Service Prov"
    cont "ided Content and"
    cont "/or Virtual Curr"
    cont "ency."
    cont "There may be Ser"
    cont "vice Provided Co"
    cont "ntent (should yo"
    cont "u choose to purc"
    cont "hase it) which w"
    cont "ill require you "
    cont "to make a paymen"
    cont "t with real mone"
    cont "y, the amount of"
    cont " which will be s"
    cont "et out in the Pr"
    cont "ogram. All Servi"
    cont "ce Provided Cont"
    cont "ent will be made"
    cont " available immed"
    cont "iately when you "
    cont "purchase it with"
    cont " real money and "
    cont "you acknowledge "
    cont "that this is the"
    cont " case and that y"
    cont "ou will have no "
    cont "right to change "
    cont "your mind and ca"
    cont "ncel (sometimes "
    cont "known as a 'cool"
    cont "ing off' right) "
    cont "once your purcha"
    cont "se is complete. "
    cont " Depending on yo"
    cont "ur platform, any"
    cont " Service Provide"
    cont "d Content purcha"
    cont "sed, will be pur"
    cont "chased from your"
    cont " platform provid"
    cont "er and such purc"
    cont "hase will be sub"
    cont "ject to their re"
    cont "spective Terms o"
    cont "f Service and Us"
    cont "er Agreement. Pl"
    cont "ease check usage"
    cont " rights for each"
    cont " purchase as the"
    cont "se may differ fr"
    cont "om item to item."
    cont " Unless otherwis"
    cont "e shown, content"
    cont " available in an"
    cont "y in-game store "
    cont "has the same age"
    cont " rating as the g"
    cont "ame."
    cont "12. AVAILABILITY"
    cont ":"
    cont "12(A) For reside"
    cont "nts in North Ame"
    cont "rica:  Activisio"
    cont "n does not guara"
    cont "ntee that any on"
    cont "line services, p"
    cont "lay or features "
    cont "associated with "
    cont "the Program (col"
    cont "lectively, 'Onli"
    cont "ne Services') or"
    cont " Service Provide"
    cont "d Content will b"
    cont "e available at a"
    cont "ll times or at a"
    cont "ny given time or"
    cont " that Inactivision"
    cont " will continue t"
    cont "o offer Online S"
    cont "ervices or Servi"
    cont "ce Provided Cont"
    cont "ent for any part"
    cont "icular length of"
    cont " time. Activisio"
    cont "n may change and"
    cont " update Online S"
    cont "ervices or Servi"
    cont "ce Provided Cont"
    cont "ent without noti"
    cont "ce to you. Activ"
    cont "ision makes no w"
    cont "arranty or repre"
    cont "sentation regard"
    cont "ing the availabi"
    cont "lity of Online S"
    cont "ervices and rese"
    cont "rves the right t"
    cont "o modify or disc"
    cont "ontinue Online S"
    cont "ervices in its s"
    cont "ole discretion w"
    cont "ithout notice, i"
    cont "ncluding for exa"
    cont "mple, ceasing an"
    cont " Online Service "
    cont "for economic rea"
    cont "sons due to a li"
    cont "mited number of "
    cont "users continuing"
    cont " to make use of "
    cont "the Online Servi"
    cont "ce over time. NO"
    cont "TWITHSTANDING AN"
    cont "YTHING TO THE CO"
    cont "NTRARY, YOU ACKN"
    cont "OWLEDGE AND AGRE"
    cont "E THAT ONLINE SE"
    cont "RVICES MAY BE TE"
    cont "RMINATED IN WHOL"
    cont "E OR IN PART AT "
    cont "INACTIVISION’S SOL"
    cont "E DISCRETION WIT"
    cont "HOUT NOTICE TO Y"
    cont "OU, AND IN CONNE"
    cont "CTION WITH ONLIN"
    cont "E SERVICES’ TERM"
    cont "INATION, ANY AND"
    cont " ALL SERVICE PRO"
    cont "VIDED CONTENT LI"
    cont "CENSED TO YOU MA"
    cont "Y BE TERMINATED."
    cont " YOU ASSUME ANY "
    cont "AND ALL RISK OF "
    cont "LOSS ASSOCIATED "
    cont "WITH THE TERMINA"
    cont "TION OF ONLINE S"
    cont "ERVICES AND ANY "
    cont "LOSS OF SERVICE "
    cont "PROVIDED CONTENT"
    cont " OTHERWISE."
    cont "12(B) For reside"
    cont "nts outside Nort"
    cont "h America: Subje"
    cont "ct to the next s"
    cont "entence, Activis"
    cont "ion does not gua"
    cont "rantee that any "
    cont "Online Services "
    cont "or Service Provi"
    cont "ded Content will"
    cont " be available or"
    cont " error-free at a"
    cont "ll times or at a"
    cont "ny given time. Ina"
    cont "ctivision warran"
    cont "ts that the Prog"
    cont "ram, in addition"
    cont " to any Service "
    cont "Provided Content"
    cont " which has been "
    cont "paid-for with re"
    cont "al money, will s"
    cont "ubstantially com"
    cont "ply with the des"
    cont "cription provide"
    cont "d by it at the p"
    cont "oint of purchase"
    cont " and be of satis"
    cont "factory quality "
    cont " (in addition an"
    cont "y related servic"
    cont "es provided thro"
    cont "ugh them will be"
    cont " provided with r"
    cont "easonable care a"
    cont "nd skill).  Inacti"
    cont "vision may chang"
    cont "e and update Onl"
    cont "ine Services or "
    cont "Service Provided"
    cont " Content without"
    cont " notice to you ("
    cont "provided always "
    cont "that any such ch"
    cont "anges do not res"
    cont "ult in material "
    cont "degradation in t"
    cont "he functionality"
    cont " of the Program "
    cont "or any Service P"
    cont "rovided Content "
    cont "which has been p"
    cont "aid-for with rea"
    cont "l money). Activi"
    cont "sion makes no wa"
    cont "rranty or repres"
    cont "entation regardi"
    cont "ng the availabil"
    cont "ity of Online Se"
    cont "rvices and/or Se"
    cont "rvice Provided C"
    cont "ontent which are"
    cont " free (i.e. not "
    cont "paid-for with re"
    cont "al money) and re"
    cont "serves the right"
    cont " to modify or di"
    cont "scontinue them i"
    cont "n its sole discr"
    cont "etion without no"
    cont "tice to you, inc"
    cont "luding for examp"
    cont "le, for economic"
    cont " reasons due to "
    cont "a limited number"
    cont " of users contin"
    cont "uing to make use"
    cont " of them over ti"
    cont "me. Inactivision i"
    cont "s not liable or "
    cont "responsible for "
    cont "any failure to p"
    cont "erform, or delay"
    cont " in performance "
    cont "of, any of its o"
    cont "bligations that "
    cont "is caused by eve"
    cont "nts outside its "
    cont "reasonable contr"
    cont "ol. If such circ"
    cont "umstances result"
    cont "ur obligation to"
    cont " make any paymen"
    cont "t to download, u"
    cont "se or access the"
    cont "m will be suspen"
    cont "ded for the dura"
    cont "tion of such per"
    cont "iod. Inactivision "
    cont "is entitled to m"
    cont "odify or discont"
    cont "inue Online Serv"
    cont "ices and/or Serv"
    cont "ice Provided Con"
    cont "tent which are p"
    cont "aid-for with rea"
    cont " in material deg"
    cont "radation in the "
    cont "functionality of"
    cont " the Program or "
    cont "Service Provided"
    cont " Content then yo"
    cont "l money in its s"
    cont "ole discretion u"
    cont "pon reasonable n"
    cont "otice to you. Th"
    cont "e warranty for s"
    cont "uch Online Servi"
    cont "ces and/or Servi"
    cont "ce Provided Cont"
    cont "ent is provided "
    cont "in accordance wi"
    cont "th your statutor"
    cont "y rights as a co"
    cont "nsumer which wil"
    cont "l always prevail"
    cont ". Please see Sec"
    cont "tion 7 in respec"
    cont "t of Inactivision'"
    cont "s limitation on "
    cont "damages, but not"
    cont "hing in this par"
    cont "agraph shall aff"
    cont "ect your statuto"
    cont "ry rights."
    cont "13. ACCESS: YOU "
    cont "ARE SOLELY RESPO"
    cont "NSIBLE FOR ANY T"
    cont "HIRD PARTY COSTS"
    cont " YOU INCUR TO US"
    cont "E THE PROGRAM AN"
    cont "D SERVICES.  You"
    cont " acknowledge and"
    cont " agree that you "
    cont "will provide at "
    cont "your own cost an"
    cont "d expense the eq"
    cont "uipment, Interne"
    cont "t, or other conn"
    cont "ection charges r"
    cont "equired to acces"
    cont "s and use the Pr"
    cont "ogram. Activisio"
    cont "n makes no warra"
    cont "nty that the Pro"
    cont "gram can be acce"
    cont "ssed or used on "
    cont "all systems, con"
    cont "trollers, or dev"
    cont "ices, by means o"
    cont "f any specific I"
    cont "nternet or other"
    cont " connection prov"
    cont "ider, or in all "
    cont "territories. The"
    cont " Program may int"
    cont "egrate, be integ"
    cont "rated into, or b"
    cont "e provided in co"
    cont "nnection with th"
    cont "ird-party servic"
    cont "es and content. "
    cont " Inactivision does"
    cont " not control tho"
    cont "se third-party s"
    cont "ervices and cont"
    cont "ent.  You should"
    cont " read the terms "
    cont "of use agreement"
    cont "s and privacy po"
    cont "licies that appl"
    cont "y to such third-"
    cont "party services a"
    cont "nd content."
    cont "14. USER GENERAT"
    cont "ED CONTENT: The "
    cont "Program may incl"
    cont "ude means by whi"
    cont "ch you and other"
    cont " users may share"
    cont " user generated "
    cont "content ('UGC')."
    cont "  To the fullest"
    cont " extent permitte"
    cont "d by applicable "
    cont "law, by submitti"
    cont "ng any UGC you a"
    cont "utomatically gra"
    cont "nt (or represent"
    cont " and warrant tha"
    cont "t the owner of s"
    cont "uch rights has e"
    cont "xpressly granted"
    cont ") Inactivision a p"
    cont "erpetual, worldw"
    cont "ide, royalty-fre"
    cont "e, irrevocable, "
    cont "non-exclusive ri"
    cont "ght and license "
    cont "to use, reproduc"
    cont "e, modify, adapt"
    cont ", publish, trans"
    cont "late, sub-licens"
    cont "e, create deriva"
    cont "tive works from "
    cont "and distribute s"
    cont "uch UGC or incor"
    cont "porate such UGC "
    cont "content into any"
    cont " form, medium, o"
    cont "r technology now"
    cont " known or later "
    cont "developed throug"
    cont "hout the univers"
    cont "e, and agree tha"
    cont "t Inactivision sha"
    cont "ll be entitled t"
    cont "o unrestricted u"
    cont "se of the UGC fo"
    cont "r any purpose wh"
    cont "atsoever, commer"
    cont "cial or otherwis"
    cont "e, without compe"
    cont "nsation (but sub"
    cont "ject to applicab"
    cont "le local legisla"
    cont "tion), notice or"
    cont " attribution.  Y"
    cont "ou waive and agr"
    cont "ee not to assert"
    cont " against Activis"
    cont "ion or any of it"
    cont "s partners, affi"
    cont "liates, subsidia"
    cont "ries or licensee"
    cont "s, any moral or "
    cont "similar rights y"
    cont "ou may have in a"
    cont "ny of your UGC. "
    cont " To the extent t"
    cont "he Program permi"
    cont "ts other users t"
    cont "o access and use"
    cont " your UGC, you a"
    cont "lso grant such u"
    cont "C subject to the"
    cont " above license."
    cont "Complaints about"
    cont " the content of "
    cont "any UGC must be "
    cont "sent to legalaff"
    cont "airs<AT>inactivision."
    cont "com and must con"
    cont "tain details of "
    cont "the specific UGC"
    cont " giving rise to "
    cont "the complaint."
    cont "15. For resident"
    cont "s in North Ameri"
    cont "ca -- COPYRIGHT "
    cont "NOTICE:"
    cont "If you believe t"
    cont "hat any content "
    cont "appearing in the"
    cont " Program and/or "
    cont "UGC has been cop"
    cont "ied in a way tha"
    cont "t constitutes co"
    cont "pyright infringe"
    cont "ment, please for"
    cont "ward the followi"
    cont "ng information t"
    cont "o the copyright "
    cont "agent named belo"
    cont "w.  Your copyrig"
    cont "ht infringement "
    cont "notification mus"
    cont "t comply with th"
    cont "e Digital Millen"
    cont "es that you may "
    done
