Script_PlayerFall:
    turnobject PLAYER, DOWN
	reloadmappart
	special UpdateTimePals
	loadvar VAR_MOVEMENT, PLAYER_FALL
	special ReplaceKrisSprite
;     jp Reset
    end
    
CheckStandingOnHole::
	ld a, [wPlayerTurningDirection]
	cp 0
	jr z, .not_hole
	cp $f0
	jr z, .not_hole
	ld a, [wPlayerStandingTile]
	call CheckHoleTile
	jr nc, .yep
	ld a, [wPlayerState]
	cp PLAYER_FALL
	jr nz, .not_hole
	
.yep
	scf
	ret

.not_hole
	and a
	ret
