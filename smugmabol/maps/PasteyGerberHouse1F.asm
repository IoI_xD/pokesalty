	object_const_def ; object_event constants
	const PASTEHOUSE_1F_PASTEY
	const PASTEHOUSE_1F_GERBER

PasteysHouse1F_MapScripts:
	db 2 ; scene scripts
	scene_script .MeetPastey ; SCENE_DEFAULT
	scene_script .DummyScene ; SCENE_FINISHED
	
	db 0 ; callbacks

.MeetPastey:
	prioritysjump MeetPastey
	end

.DummyScene:
	end

PasteysHouse1F_MapEvents:
	db 0, 0 ; filler

	db 3 ; warp events
	warp_event  2, 7, ROUTE_47, 1
	warp_event  3, 7, ROUTE_47, 1
  	warp_event  7, 0, PASTEYS_HOUSE_2F, 1

	db 0 ; coord events
	
	db 0 ; bg events

	db 2 ; object events
    object_event  2,  4, SPRITE_PASTEY, SPRITEMOVEDATA_STANDING_RIGHT, 0, 0, -1, -1, 0, OBJECTTYPE_SCRIPT, 0,DefaultScript, EVENT_MEETPASTEYANDGERBER
	object_event  3,  4, SPRITE_GERBER, SPRITEMOVEDATA_STANDING_LEFT, 0, 0, -1, -1, 0, OBJECTTYPE_SCRIPT, 0, DefaultScript, EVENT_MEETPASTEYANDGERBER
;EVENT_MEETPASTEYANDGERBER
    
DefaultScript:
    jumptext Default_Text
	
MeetPastey:
    showemote EMOTE_SHOCK, PASTEHOUSE_1F_PASTEY, 15
    turnobject PASTEHOUSE_1F_PASTEY, DOWN
    opentext
	writetext MeetPastey_Text
	waitbutton
	closetext
    turnobject PASTEHOUSE_1F_GERBER, DOWN
    opentext 
	writetext MeetGerber_Text
	waitbutton
	closetext
	ismorn
	iftrue .morn
	isday
	iftrue .day
	isnite
	iftrue .nite
	opentext
	writetext PasteyWhat_Text
	waitbutton
	closetext
	end
.morn
	opentext
    writetext PasteyMorn_Text
	waitbutton
	closetext
.day
	opentext
    writetext PasteyDay_Text
	waitbutton
	closetext
.nite
	opentext
    writetext PasteyNight_Text
	waitbutton
	closetext
	
MeetPastey_Text:
    text "PASTEY: Oh hey"
    line "<PLAYER>, glad you "
	cont "remembered."
    cont "Today's gonna"
    cont "be an epic fuckin'"
	cont "day, isn't it?"
    cont "We're finally"
    cont "gonna get off our"
    cont "asses and do"
    cont "something with"
    cont "our lives."
    cont "Wonderful!"
    done
    
MeetGerber_Text:
    text "GERBER: I'll kill"
    line "all of you."
    done
    
PasteyMorn_Text:
    text "PASTEY: See?"
    line "That's the"
    cont "spirit."
    cont "Anyways, the"
    cont "professer's"
    cont "lab opens"
    cont "soon so we"
    cont "probably want"
    cont "to get there"
    cont "soon."
    done
    
PasteyDay_Text:
    text "PASTEY: See?"
    line "That's the"
    cont "spirit."
    cont "Anyways, we've"
    cont "waited long"
    cont "enough. We"
    cont "should really"
    cont "get to the"
    cont "professer's"
    cont "lab soon."
    
PasteyNight_Text:
    text "PASTEY: See?"
    line "That's the"
    cont "spirit."
    cont "Anyways, I'm"
    cont "pretty sure"
    cont "the lab is"
    cont "still open"
    cont "despite it"
    cont "being <TIME>…"
    done
	
GerberBattle_Text:
	text "Wait a second…"
    
PasteyWhat_Text:
	text "PASTEY: So,"
	line "funny story…"
	cont "I don't know"
	cont "what time it"
	cont "is. In fact,"
	cont "nobody does."
	cont "If you're"
	cont "seeing this,"
	cont "there was an"
	cont "error in the"
	cont "game. Somehow,"
	cont "all the tests"
	cont "to check what"
	cont "time it is in"
	cont "the game's"
	cont "code...just..."
	cont "failed..."
	
	para "GERBER: The"
	line "fuck are you"
	cont "talking about,"
	cont "Pastey?"
	
	para "Oops, forgot"
	line "nobody else can"
	cont "break the fourth"
	cont "wall. Anyways..."
	done
	
Default_Text:
    text "C'mon! Let's"
    line "go!"
    done
