	object_const_def ; object_event constants
	const RANDOMGUYHOUSE_OLDGUY

RandomGuyHouse_MapScripts:
	db 0 ; scene scripts
	
	db 0 ; callbacks

RandomGuyHouse_MapEvents:
	db 0, 0 ; filler

	db 2 ; warp events
	warp_event  0, 5, ROUTE_47, 5
	warp_event  1, 5, ROUTE_47, 5

	db 0 ; coord events
	
	db 0 ; bg events

	db 1 ; object events
    object_event  2,  4, SPRITE_GRAMPS, SPRITEMOVEDATA_STANDING_LEFT, 0, 0, -1, -1, 0, OBJECTTYPE_SCRIPT, 0,DefaultRandomGuyScript, -1

DefaultRandomGuyScript:
    jumptext DefaultRandomGuy_Text

DefaultRandomGuy_Text:
    text "I've been living"
	cont "in this house"
	line "for the past 50"
	line "years, sitting"
	line "in this chair,"
	line "waiting for the"
	line "day when some"
	line "random ass kid"
	line "would walk in"
	line "and talk to me."
	line "You have no"
	line "idea…"
	line "I've lived in"
	line "this house for"
	line "my entire life,"
	line "and I've never"
	line "seen anything"
	line "beyond this"
	line "patch of land."
	line "Anything, that"
	line "is, except for"
	line "a forest."
	line "When I was 7, I"
	line "was exploring"
	line "the area when"
	line "I found a red"
	line "carpet. The"
	line "moment I stood"
	line "on that carpet"
	line "I was thrust"
	line "into this"
	line "mystical forest."
	line "The foliage was"
	line "an aqua blue…"
	line "all around me"
	line "were hoards of"
	line "creatures that"
	line "I had never"
	line "seen before…"
	line "I had never"
	line "seen anything"
	line "like it!"
	line "I decided to"
	line "climb the"
	line "only hill in"
	line "the forest,"
	line "and I"
	line "found myself"
	line "in front of"
	line "a ravine."
	line "From across"
	line "the ravine,"
	line "I saw a…"
	line "…humungous"
	line "blue creature"
	line "sleeping…"
	line "I ran back to"
	line "my house,"
	line "and I've never"
	line "been back…"
	line "because I've"
	line "never been"
	line "able to find"
	line "the forest."
	line "While you're"
	line "here, you"
	line "may want to"
	line "find that"
	line "forest…it may"
	line "have something"
	line "you're looking"
	line "for."
    done
