	object_const_def ; object_event constants
	const ROUTE_47_SCRIPTS

Route47_MapScripts:
	db 0 ; scene scripts

	db 0 ; callbacks

Route47_MapEvents: 
	db 0, 0 ; filler

	db 5 ; warp events
	warp_event 2, 40, ROUTE47TOLORETOWN, 1
	warp_event 2, 41, ROUTE47TOLORETOWN, 2
	warp_event 19, 7, PASTEYS_HOUSE_1F, 1
	warp_event 10, 51, SECRET_CAVE_1, 1
	warp_event 47, 15, RANDOM_GUY_HOUSE, 1
;	warp_event 57, 26, SECRET_FOREST, 1
;	warp_event 57, 27, SECRET_FOREST, 2

	db 0 ; coord events

	db 4 ; bg events
	bg_event 43, 53, BGEVENT_READ, ControlsSign
	bg_event 20, 7, BGEVENT_READ, PGHouseSign 
	bg_event 12, 19, BGEVENT_READ, LedgeSign
	bg_event 4, 25, BGEVENT_READ, SecretCaveSign
;	bg_event 48, 15, BGEVENT_READ, RandomGuySign
; Kaze: "The next thing I say will be in your rom hack.
	db 0 ; object events


ControlsSign:
    jumptext Text_Controls

PGHouseSign:
    jumptext Text_PGHouse

LedgeSign:
	jumptext Text_LedgeSign
	
SecretCaveSign:
	jumptext Text_SecretCave

Text_PGHouse:
    text "Pastey and"
	line "Gerber's House"
    done

Text_Controls:
    text "   OBLIGATORY"
    line "FOURTH WALL BREAK"
    cont " THAT TELLS YOU"
    cont " ALL THE CONTROLS"
    
    para "Other then the"
    line "obvious ones,"
    cont "like using the"
    cont "D-PAD to move,"
    cont "you can hold <BOLD_B>"
    cont "while moving to"
    cont "run."
    
    para "You may think"
    line "those holes over"
    cont "there will kill "
    cont "you if you walk"
    cont "over them, and"
    cont "you're right,"
    cont "you'll fall in"
    cont "and break your"
    cont "bones lmao."
    cont "That's why"
    cont "you can press <BOLD_A>"
    cont "while walking, and"
    cont "while not facing"
    cont "an NPC or sign"
    cont "to jump."
    
    para "<BOLD_B> and <BOLD_A> can be"
    line "combined for a"
    cont "running jump, "
    cont "which is useful "
    cont "for speedrunning,"
    cont "among other"
    cont "things."
	
	para " DEVELOPER NOTES"
	line "There's a known"
	cont "bug where if you"
	cont "jump into a route"
	cont "or town, it will"
	cont "not fully load"
	cont "and your player"
	cont "will disappear."
	cont "Fixing this is"
	cont "out of my"
	cont "expertise, so"
	cont "if this happens"
	cont "simply walk back"
	cont "into the last"
	cont "area and then"
	cont "walk into the"
	cont "next area."
	
	para "Please don't ask"
	line "how I fit this"
	cont "much shit onto a"
	cont "sign…"
    done

Text_LedgeSign:
	para " DEVELOPER NOTES"
	line "Ledges cout as"
	cont "walls as well."
	cont "Since you can't"
	cont "jump next to"
	cont "them, you'll"
	cont "have to jump"
	cont "onto them from"
	cont "a distance."
	para "Don't expect too"
	line "many of these"
	cont "notes to appear"
	cont "throughout the"
	cont "game."
	done
	
Text_SecretCave:
	para "The sign is"
	line "faded, but you"
	cont "can faintly make"
	cont "something out."
	para " YOU FOUND…YE"
	line "HIDDEN CAVE…………"
	cont "IF YOU CAN GET"
	cont "THROUGH…BOOBY"
	cont "TRAPS……FIND"
	cont "SACRED TREASURE"
	cont "………………………………"
	para "You look into"
	line "the cave and"
	cont "see that there"
	cont "is actually a"
	cont "ladder in the"
	cont "hole that"
	cont "goes downward."
	done
	