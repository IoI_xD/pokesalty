	object_const_def ; object_event constants
	const SALTY_YARD

SaltyYard_MapScripts:
	db 0 ; scene scripts

	db 0 ; callbacks

SaltyYard_MapEvents:
	db 0, 0 ; filler

	db 1 ; warp events
	warp_event  5, 9, SALTYS_HOUSE, 1

	db 0 ; coord events

	db 1 ; bg events
	bg_event 6, 9, BGEVENT_READ, HouseSign

	db 0 ; object events

HouseSign:
    jumptext Text_House
    
Text_House:
    text "<PLAYER>'s house"
    line ""
    done
    

