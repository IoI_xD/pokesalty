	object_const_def ; object_event constants
	const SALTY_HOUSE

SaltyHouse_MapScripts:
	db 0 ; scene scripts

	db 0 ; callbacks

SaltyHouse_MapEvents:
	db 0, 0 ; filler

	db 3 ; warp events
	warp_event  2, 7, SALTYS_YARD, 1
    warp_event  3, 7, SALTYS_YARD, 1
    warp_event  7, 0, SALTYS_ROOM, 1

	db 0 ; coord events

	db 5 ; bg events
	bg_event 0, 1, BGEVENT_READ, TV_Text
 	bg_event 1, 1, BGEVENT_READ, Book1Text
	bg_event 2, 1, BGEVENT_READ, Book2Text
	bg_event 3, 1, BGEVENT_READ, Book3Text
	bg_event 4, 0, BGEVENT_READ, EmptyFridgeText
	bg_event 5, 0, BGEVENT_READ, CalendarText

	db 0 ; object events

TV_Text:
    jumptext Text_TV
    
Book1Text:
	faceplayer
	opentext
	writetext Text_Book1
	yesorno
	iffalse .No
 	writetext Text_Book1Yes
 	waitbutton
 	closetext
	end
	
.No:
 	writetext Text_Book1No
 	waitbutton
	closetext
	end
    
Book2Text:
    jumptext Text_Book2
    
Book3Text:
    jumptext Text_Book3
	
CalendarText:
    jumptext Text_Calendar

EmptyFridgeText:
    jumptext Text_Fridge
    
Text_TV:
    text "There's nothing"
    line "playing on the"
    cont "TV. It's just"
    cont "static."
    done
    
Text_Book1:
    text "The shelves are"
    line "loaded with the"
    cont "latest #MON"
    cont "hentai."
    
    para "Take one out…?"
    done

Text_Book1No:
    text "…your dick"
    line "retracted back"
    cont "into it's place."
    done
    
Text_Book1Yes:
    text "…you took one"
    line "out. It's titled"
    cont "“Luigi's Thick Ass”"
    cont "It's not #MON"
    cont "related, but"
    cont "honestly, it's"
    cont "probably ten"
    cont "times better."
    
    para "You feel the urge"
    line "to check your"
    cont "calendar first,"
    cont "before reading it."
    done
    
Text_Book2:
    text "There's one book."
    line "“Alvin and the"
    cont "Chipmunks: The" 
    cont "Junior Novel”."
    cont "It's a good thing"
    cont "the bible is"
    cont "displayed so"
    cont "proudly in your"
    cont "house."
    done
    
Text_Book3:
    text "There aren't even"
    line "any cobwebs here,"
    cont "that's how empty"
    cont "this shelf is."
    done

Text_Calendar:
    text "The date is"
    line "March 10th."
    cont "“#MON QUEST”"
    cont "is written on"
    cont "this day."
    cont "…ah yes, that's"
    cont "right. You,"
    cont "Pastey, and "
    cont "Gerber all agreed"
    cont "to start a"
    cont "#MON"
    cont "journey today."
    cont "They're probably"
    cont "waiting at their"
    cont "house for you."
    done

Text_Fridge:
    text "There's the batch"
    line "of cookies you"
    cont "baked live on"
    cont "stream a few"
    cont "weeks ago."
    cont "Other then that,"
    cont "there's a box of"
    cont "cold, half-eaten"
    cont "pizza, one can of"
    cont "Sprite, a rotten"
    cont "jug of milk, and"
    cont "a dust bunny."
